import plugin from '../../lib/plugins/plugin.js'
import fetch from "node-fetch";
import{segment}from'oicq'
import _ from 'lodash'
import cfg from '../../lib/config/config.js'
const api='https://api.lolimi.cn/API/ark/a.php'
/*卡片外显，可随意添加，也可直接加入指令*/
const xx='震惊.zip'
export class example extends plugin {
    constructor() {
        super({
            /** 功能名称 */
            name: 'img',
            /** 功能描述 */
            dsc: 'image',
            /** https://oicqjs.github.io/oicq/#events */
            event: 'message',
            /** 优先级，数字越小等级越高 */
            priority: 1,
            rule: [


                {
                    reg: "^#?转大图(.*)$", //匹配消息正则，命令正则
                    fnc: 'ImageLink'
                }
            ]
        })
    }
    async ImageLink(e) {
        let img = []
        let yx
        var reg = /{[^}]*}/g
        let wx=this.e.toString().replace(/(\s|#?转大图|{[^}]*})/g,'');
        if(wx){
            	yx=wx;
        }
        else{
        	yx=xx
        }
        if (e.source) {
            let source
            if (e.isGroup) {
                source = (await e.group.getChatHistory(e.source.seq, 1)).pop()
            } else {
                source = (await e.friend.getChatHistory(e.source.time, 1)).pop()
            }
            for (let i of source.message) {
                if (i.type == 'image') {
                    img.push(i.url)
                }
            }
        } else {
            img = e.img
        }

        if (_.isEmpty(img)) {

            this.setContext('_ImageLinkContext')
            await this.reply('⚠ 请发送图片')
            return
        }
        if (img.length >= 2) {
            for (let i of img) {
            	let url=api+`?img=${i}&yx=${yx}`
            	let res = await fetch(url).catch((err) => logger.error(err));
		if (!res) {
		        logger.error('查询接口请求失败');
		        return await this.reply('查询接口请求失败');
		    }
		res = await res.json();
		logger.info("请求结果：成功");
		await this.e.reply(segment.json(res))
            }
        } 
        else {
            let url=api+`?img=${img[0]}&yx=${yx}`
            let res = await fetch(url).catch((err) => logger.error(err));
            if (!res) {
                logger.error('查询接口请求失败');
                return await this.reply('查询接口请求失败');
            }
            res = await res.json();
            logger.info("请求结果：成功");
            await this.e.reply(segment.json(res))
        }
        return true
    }

    async _ImageLinkContext() {
        let img = this.e.img
        let yx
        var reg = /{[^}]*}/g
        let wx=this.e.toString().replace(/(\s|#?转大图|{[^}]*})/g,'');
        if(wx){
            	yx=wx;
        }
        else{
        	yx=xx;
        }
        if (this.e.msg === '取消') {
            this.finish('_ImageLinkContext')
            await this.reply('✅ 已取消')
            return
        }
        if (!img) {
            this.setContext('_ImageLinkContext')
            await this.reply('⚠ 请发送图片或取消')
            return
        }
        let url=api+`?img=${img[0]}&yx=${yx}`
        let res = await fetch(url).catch((err) => logger.error(err));
        if (!res) {
            logger.error('查询接口请求失败');
            return await this.reply('查询接口请求失败');
        }
        res = await res.json();
        logger.info("请求结果：成功");
        await this.e.reply(segment.json(res))
        this.finish('_ImageLinkContext')
    }
}