import plugin from '../../lib/plugins/plugin.js'
import fetch from 'node-fetch'
import { segment } from "oicq";
import lodash from "lodash";
//插件作者：风  qq：459521475
//先去除了所有的涉及到敏感信息的功能
const groupimg='https://tenapi.cn/v2/groupimg';
const spaceimg='https://tenapi.cn/v2/qzoneimg';
const bilibiliweekly='https://tenapi.cn/v2/weekly';
const acg='https://tenapi.cn/v2/acg';
const dm='https://api.ayao.ltd/head-portrait/api.php';
const pcdm='https://api.ayao.ltd/PC/api.php';
const mbdm='https://api.ayao.ltd/Mobile/api.php';
const tiktokhot='https://tenapi.cn/v2/douyinhot';
const weibohot='https://tenapi.cn/v2/weibohot';
const bilibilihot='https://tenapi.cn/v2/bilihot';
const toutiaohot='https://tenapi.cn/v2/toutiaohot';
const toutiaohotnews='https://tenapi.cn/v2/toutiaohotnew';
const zhihu='https://tenapi.cn/v2/zhihuhot';
const baiduhot='https://tenapi.cn/v2/baiduhot';


export class slander extends plugin {
    constructor() {
        super({
            name: '信息查询',
            dsc: '查询一些无用的信息',
            /** https://oicqjs.github.io/oicq/#events */
            event: 'message',
            priority: 100,
            rule: [
                {
                    reg: '^#?群头像(.*)$',
                    fnc: 'groupimg'
                },
                {
                    reg: '^#?空间头像(.*)$',
                    fnc: 'spaceimg'
                },
                {
                    reg: '^#?(bilibili|哔哩哔哩)周榜(.*)$',
                    fnc: 'bilibili'
                },
                {
                    reg: '^#?(bilibili|哔哩哔哩)(热搜榜|热榜|热度榜|热搜)$',
                    fnc: 'bilibilihot'
                },
                {
                    reg: '^#?(抖音|tiktok)(热搜榜|热榜|热度榜|热搜)$',
                    fnc: 'tiktok'
                },
                {
                    reg: '^#?微博(热搜榜|热榜|热度榜|热搜)$',
                    fnc: 'weibo'
                },
                {
                    reg: '^#?今日头条(热搜榜|热榜|热度榜|热搜)$',
                    fnc: 'toutiao'
                },
                {
                    reg: '^#?今日头条(新闻|热点新闻)$',
                    fnc: 'toutiaohotnews'
                },
                {
                    reg: '^#?知乎(热搜榜|热榜|热度榜|热搜)$',
                    fnc: 'zhihu'
                },
                {
                    reg: '^#?(百度|baidu)(热搜榜|热榜|热度榜|热搜)$',
                    fnc: 'baiduhot'
                },
                {
                    reg: '^#?(随机)?(动漫图|acg)$',
                    fnc: 'acg'
                },
                {
                    reg: '^#?(二次元头像|动漫头像|头像)$',
                    fnc: 'dm'
                },
                {
                    reg: '^#?(PC|pc|电脑)(动漫图|二次元图|壁纸)$',
                    fnc: 'pcdm'
                },
                {
                    reg: '^#?手机(动漫图|二次元图|壁纸)$',
                    fnc: 'mbdm'
                }
            ]
        })
    }
    async groupimg(e){
		logger.info('[用户命令]', e.msg);
		let qun;
		let qunreg = /[1-9][0-9]{4,12}/;
		let qunret = qunreg.exec(e.toString());
		if (qunret&&!e.at){qun=qunret}
		else{qun=this.e.group_id}
		let url = groupimg + `?qun=${qun}`;
		e.reply(segment.image(url));
    }
    async spaceimg(e){
		logger.info('[用户命令]', e.msg);
		let qq;
		let qqreg = /[1-9][0-9]{4,12}/;
		let qqret = qqreg.exec(e.toString());
		if (qqret&&!e.at){qq=qqret}
		else if (e.at){qq=e.at}
		else{qq=this.e.user_id}
		let url = spaceimg + `?qq=${qq}`;
		e.reply(segment.image(url));
    }
    async bilibili(e){
		logger.info('[用户命令]', e.msg);
		let num;
		let url;
		let numreg = /[1-9][0-9]{4,12}/;
		let numret = numreg.exec(e.toString());
		if(numret){url=bilibiliweekly + `?num=${num}`}
		else{url=bilibiliweekly}
		let res = await fetch(url).catch((err) => logger.error(err));
		if (!res) {
			logger.error('查询接口请求失败');
			return await this.reply('查询接口请求失败');
        	}
        	res = await res.json();
        	logger.info(`请求结果：${res.msg}`);
        	if(res.msg=="success"){
			e.reply("查询成功");
			let MsgList = [];
			let msg=[]
			let j = res.data.list.length
			MsgList.push({
						message: `当前查询期数:${res.data.name}`,
						nickname: e.nickname,
						user_id: e.user_id
					});
			MsgList.push({
						message: `若是想要查询其他期的bilibili周榜请在指令后加上期数(例如bilibili周榜123)，否则默认为最新`,
						nickname: e.nickname,
						user_id: e.user_id
					});
			for (let i = 0 ;i<j;i++){
				MsgList.push({
						message: [segment.image(res.data.list[i].cover),"\n标题:",res.data.list[i].title,"\n推荐理由:",res.data.list[i].reason,"\n直达链接:",res.data.list[i].url],
						nickname: e.nickname,
						user_id: e.user_id
					});	
			}
			let forwardMsg = await e.group.makeForwardMsg(MsgList);
			forwardMsg.data = forwardMsg.data
      .replace(/\n/g, '')
      .replace(/<title color="#777777" size="26">(.+?)<\/title>/g, '___')
      .replace(/___+/, `<title color="#777777" size="26">${res.data.name}哔哩哔哩周榜～</title>`)
    			await e.reply(forwardMsg)
    		}
    		else return e.reply(res.msg)
    }
    async bilibilihot(e){
		logger.info('[用户命令]', e.msg);
		let url=bilibilihot
		let res = await fetch(url).catch((err) => logger.error(err));
		if (!res) {
			logger.error('查询接口请求失败');
			return await this.reply('查询接口请求失败');
        	}
        res = await res.json();
        logger.info(`请求结果：${res.msg}`);
        if(res.msg=="success"){
			e.reply("查询成功");
			let MsgList = [];
			let msg=[]
			let j = res.data.length
			for (let i = 0 ;i<j;i++){
				MsgList.push({
						message: ["标题:",res.data[i].name,"\n直达链接:",res.data[i].url],
						nickname: e.nickname,
						user_id: e.user_id
					});	
			}
			let forwardMsg = await e.group.makeForwardMsg(MsgList);
			forwardMsg.data = forwardMsg.data
      .replace(/\n/g, '')
      .replace(/<title color="#777777" size="26">(.+?)<\/title>/g, '___')
      .replace(/___+/, `<title color="#777777" size="26">bilibili热搜榜～</title>`)
    			await e.reply(forwardMsg)
    		}
    		else return e.reply(res.msg)
    }
    async tiktok(e){
		logger.info('[用户命令]', e.msg);
		let url=tiktokhot;
		let res = await fetch(url).catch((err) => logger.error(err));
		if (!res) {
			logger.error('查询接口请求失败');
			return await this.reply('查询接口请求失败');
        	}
        res = await res.json();
        logger.info(`请求结果：${res.msg}`);
        if(res.msg=="success"){
			e.reply("查询成功");
			let MsgList = [];
			let msg=[]
			let j = res.data.length
			for (let i = 0 ;i<j;i++){
				MsgList.push({
						message: ["标题:",res.data[i].name,"\n热度:",res.data[i].hot,"\n直达链接:",res.data[i].url],
						nickname: e.nickname,
						user_id: e.user_id
					});	
			}
			let forwardMsg = await e.group.makeForwardMsg(MsgList);
			forwardMsg.data = forwardMsg.data
      .replace(/\n/g, '')
      .replace(/<title color="#777777" size="26">(.+?)<\/title>/g, '___')
      .replace(/___+/, `<title color="#777777" size="26">tiktok热搜榜～</title>`)
    			await e.reply(forwardMsg)
    		}
    		else return e.reply(res.msg)
    }
    async weibo(e){
		logger.info('[用户命令]', e.msg);
		let url=weibohot
		let res = await fetch(url).catch((err) => logger.error(err));
		if (!res) {
			logger.error('查询接口请求失败');
			return await this.reply('查询接口请求失败');
        	}
        res = await res.json();
        logger.info(`请求结果：${res.msg}`);
        if(res.msg=="success"){
			e.reply("查询成功");
			let MsgList = [];
			let msg=[]
			let j = res.data.length
			for (let i = 0 ;i<j;i++){
				MsgList.push({
						message: ["标题:",res.data[i].name,"\n热度:",res.data[i].hot,"\n直达链接:",res.data[i].url],
						nickname: e.nickname,
						user_id: e.user_id
					});	
			}
			let forwardMsg = await e.group.makeForwardMsg(MsgList);
			forwardMsg.data = forwardMsg.data
      .replace(/\n/g, '')
      .replace(/<title color="#777777" size="26">(.+?)<\/title>/g, '___')
      .replace(/___+/, `<title color="#777777" size="26">微博热搜榜～</title>`)
    			await e.reply(forwardMsg)
    		}
    		else return e.reply(res.msg)
    }
    async toutiao(e){
		logger.info('[用户命令]', e.msg);
		let url=toutiaohot
		let res = await fetch(url).catch((err) => logger.error(err));
		if (!res) {
			logger.error('查询接口请求失败');
			return await this.reply('查询接口请求失败');
        	}
        res = await res.json();
        logger.info(`请求结果：${res.msg}`);
        if(res.msg=="success"){
			e.reply("查询成功");
			let MsgList = [];
			let msg=[]
			let j = res.data.length
			for (let i = 0 ;i<j;i++){
				MsgList.push({
						message: ["标题:",res.data[i].name,"\n直达链接:",res.data[i].url],
						nickname: e.nickname,
						user_id: e.user_id
					});	
			}
			let forwardMsg = await e.group.makeForwardMsg(MsgList);
			forwardMsg.data = forwardMsg.data
      .replace(/\n/g, '')
      .replace(/<title color="#777777" size="26">(.+?)<\/title>/g, '___')
      .replace(/___+/, `<title color="#777777" size="26">今日头条热搜榜～</title>`)
    			await e.reply(forwardMsg)
    		}
    		else return e.reply(res.msg)
    }
    async toutiaohotnews(e){
		logger.info('[用户命令]', e.msg);
		let url=toutiaohotnews
		let res = await fetch(url).catch((err) => logger.error(err));
		if (!res) {
			logger.error('查询接口请求失败');
			return await this.reply('查询接口请求失败');
        	}
        res = await res.json();
        logger.info(`请求结果：${res.msg}`);
        if(res.msg=="success"){
			e.reply("查询成功");
			let MsgList = [];
			let msg=[]
			let j = res.data.length
			for (let i = 0 ;i<j;i++){
				MsgList.push({
						message: ["标题:",res.data[i].name,"\n直达链接:",res.data[i].url],
						nickname: e.nickname,
						user_id: e.user_id
					});	
			}
			let forwardMsg = await e.group.makeForwardMsg(MsgList);
			forwardMsg.data = forwardMsg.data
      .replace(/\n/g, '')
      .replace(/<title color="#777777" size="26">(.+?)<\/title>/g, '___')
      .replace(/___+/, `<title color="#777777" size="26">今日头条热点新闻～</title>`)
    			await e.reply(forwardMsg)
    		}
    		else return e.reply(res.msg)
    }
    async zhihu(e){
		logger.info('[用户命令]', e.msg);
		let url=zhihu
		let res = await fetch(url).catch((err) => logger.error(err));
		if (!res) {
			logger.error('查询接口请求失败');
			return await this.reply('查询接口请求失败');
        	}
        res = await res.json();
        logger.info(`请求结果：${res.msg}`);
        if(res.msg=="success"){
			e.reply("查询成功");
			let MsgList = [];
			let msg=[]
			let j = res.data.length
			for (let i = 0 ;i<j;i++){
				MsgList.push({
						message: ["标题:",res.data[i].name,"\n热度:",res.data[i].hot,"\n直达链接:",res.data[i].url],
						nickname: e.nickname,
						user_id: e.user_id
					});	
			}
			let forwardMsg = await e.group.makeForwardMsg(MsgList);
			forwardMsg.data = forwardMsg.data
      .replace(/\n/g, '')
      .replace(/<title color="#777777" size="26">(.+?)<\/title>/g, '___')
      .replace(/___+/, `<title color="#777777" size="26">知乎热搜榜～</title>`)
    			await e.reply(forwardMsg)
    		}
    		else return e.reply(res.msg)
    }
    async baiduhot(e){
		logger.info('[用户命令]', e.msg);
		let url=baiduhot
		let res = await fetch(url).catch((err) => logger.error(err));
		if (!res) {
			logger.error('查询接口请求失败');
			return await this.reply('查询接口请求失败');
        	}
        res = await res.json();
        logger.info(`请求结果：${res.msg}`);
        if(res.msg=="success"){
			e.reply("查询成功");
			let MsgList = [];
			let msg=[]
			let j = res.data.length
			for (let i = 0 ;i<j;i++){
				MsgList.push({
						message: ["标题:",res.data[i].name,"\n热度:",res.data[i].hot,"\n直达链接:",res.data[i].url],
						nickname: e.nickname,
						user_id: e.user_id
					});	
			}
			let forwardMsg = await e.group.makeForwardMsg(MsgList);
			forwardMsg.data = forwardMsg.data
      .replace(/\n/g, '')
      .replace(/<title color="#777777" size="26">(.+?)<\/title>/g, '___')
      .replace(/___+/, `<title color="#777777" size="26">百度热搜榜～</title>`)
    			await e.reply(forwardMsg)
    		}
    		else return e.reply(res.msg)
    }
    async acg(e){
    	e.reply(segment.image(acg));
    }
    async dm(e){
    	e.reply(segment.image(dm));
    }
    async pcdm(e){
    	e.reply(segment.image(pcdm));
    }
    async mbdm(e){
    	e.reply(segment.image(mbdm));
    }
  }