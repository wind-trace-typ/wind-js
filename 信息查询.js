import plugin from '../../lib/plugins/plugin.js'
import { segment } from "oicq"
import fetch from 'node-fetch'
export class example extends plugin {
  constructor () {
    super({
      name: 'QQ信息查询',
      dsc: 'QQ信息获取',
      event: 'message',
      priority: 10,
      rule: [
        {
            reg: `^#?查询信息(.*)`,
            fnc: 'level'
        },
      ]
    })
  }
    async level (e) {
      logger.info('[用户命令]', e.msg);
      let qq;
      let QQreg = /[1-9][0-9]{4,12}/;
      let QQret = QQreg.exec(e.msg);
      if (QQret&&!e.at){qq=QQret}
      else if(e.at){qq=e.at}
      else {qq=e.user_id}
      let url = `https://api.kit9.cn/api/qq_material/api.php?qq=${qq}`;
      let res = await fetch(url).catch((err) => logger.error(err))
      res = await res.json()
      if(res.code === 200 ) {
		logger.info('【查询成功】', e.msg);
		await e.reply([segment.image(res.data.imgurl),"\n",segment.at(e.user_id),`查询成功
昵称:${res.data.name},
签名:${res.data.sign},
年龄:${res.data.age},
性别:${res.data.gender},
来自:${res.data.country},${res.data.province},${res.data.city},
点赞量:${res.data.clike},
qq等级:${res.data.level}
`])
      }
      if (!res) {
        logger.error('【查询失败】')
        return await this.reply(`查询失败,未输入正确账号`,true)
      }
      }
}