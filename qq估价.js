import plugin from '../../lib/plugins/plugin.js'
import fetch from 'node-fetch'
import { segment } from "oicq";
import lodash from "lodash";
//插件作者：风  qq：459521475
const api = 'https://api.lolimi.cn/api/qgj/index';
/*在桑帛云https://api.lolimi.cn/注册登录后在控制台得到的的key(免费)（必须）*/
const key='这里填你注册的key';
export class example extends plugin {
    constructor() {
        super({
            /** 功能名称 */
            name: 'qq估价',
            /** 功能描述 */
            dsc: 'qq估价',
            /** https://oicqjs.github.io/oicq/#events */
            event: 'message',
            /** 优先级，数字越小等级越高 */
            priority: 1,
            rule: [

                {
                    reg: "^#?(qq|QQ)?估价(.*)$", //匹配消息正则，命令正则
                    fnc: 'order'
                }
            ]
        })
    }

    async order (e){
        logger.info('[qq估价.js]', e.msg);
        let qq;
        let qqreg = /[1-9][0-9]{0,12}/;
        let qqret= qqreg.exec(e.toString());
        if(e.at){
            qq=e.at;
        }
        else if(!e.at&&qqret){
            qq=qqret;
        }
        else {
            qq = e.user_id
        }
        let url = api +`?key=${key}&qq=${qq}`; 
        await e.reply(segment.image(url));
    }
}