# wind-js

### 访问量

[![访问量](https://profile-counter.glitch.me/wind-js/count.svg)](https://gitee.com/wind-trace-typ/wind-js)

#### 介绍

这些js后面可能不会经常更新，请移步[wind-plugin](https://gitee.com/wind-trace-typ/wind-plugin),这里更新会比较及时，且部分功能已经迁移到plugin中。

1.   各大榜单及动漫图查询.js

本插件中使用了一些免费的api获取各种信息，如群头像，空间头像，哔哩哔哩、抖音、微博、今日头条、知乎、百度等平台的热搜榜单，二次元图，电脑二次元壁纸，手机二次元壁纸，动漫头像等

2.   戳我戳他.js

本插件可以让机器人对自己或者被艾特的人发出自定义次数的连戳，且插件中可以自定义戳戳的次数，限制的最高次数(建议调少)，戳戳的间隔时间(建议调高)，这种没有限制的戳戳可能会有一点点的封号的风险，请自行决定是否使用

3.   才字诀.js

本插件可识别出别人发出的以'你'开头的句子(单个你字不触发)然后将'你'替换成'你才'回复出去，有一些可爱，但是如果觉得烦了也可以自行设置是否需要艾特机器人才能触发

4.   自嘲嘲笑.js

本插件可以生成使用者或者被艾特者自己嘲笑自己的聊天记录，适合捉弄群友，且插件中可自定义禁止嘲笑的qq，嘲笑的词条，还有自己自嘲或者被嘲笑时的回复词条，默认的嘲笑词条比较温和，不必担心玩过头群友生气

5.   随机图片.js

本插件可以发送自己存在Miao-Yunzai/resources/wind-img或者Yunzai-Bot/resources/wind-img文件夹中的jpg格式和png格式的图片，可以自行创建一个新的文件夹去防止自己的图片，且不用重命名图片，每次发送都是随机的。也可拉去我的wind-img文件夹

6.   做菜系统.js

本插件可以获取到你需要做的菜的做法，以聊天记录的形式发出，配备图片教学过程，且可自定义发送的教程数量(最大10个)

7.   转大图.js(本插件使用的api不是很稳定，有时会无法使用)

本插件可以将你选中的图片以大图卡片的形式发送出来，可以自定义卡片外显，效果如下
![大致效果如下](wind-img/IMG_20230902_120950.jpg)
![卡片外显](wind-img/IMG_20230902_122148.jpg)

8.   qq估价.js

对你或群友的qq进行估价，返回估价图片

#### 反馈方式

请直接提[Issues](https://gitee.com/wind-trace-typ/wind-js/issues)或者qq联系我[459521475](https://tenapi.cn/v2/qqcard?qq=459521475)，有任何建议也可直接提出，我会尽力修改

#### 安装教程

请在云崽或喵崽根目录分别执行以下拉取命令，可自行按需拉取，用哪些装哪些

1.  各大榜单及动漫图查询

```
curl -o "./plugins/example/各大榜单及动漫图查询.js" "https://gitee.com/wind-trace-typ/wind-js/raw/master/各大榜单及动漫图查询.js"
```

2.  戳我戳他

```
curl -o "./plugins/example/戳我戳他.js" "https://gitee.com/wind-trace-typ/wind-js/raw/master/戳我戳他.js"
```

3.  才字诀

```
curl -o "./plugins/example/才字诀.js" "https://gitee.com/wind-trace-typ/wind-js/raw/master/才字诀.js"
```
4.  自嘲嘲笑

```
curl -o "./plugins/example/自嘲嘲笑.js" "https://gitee.com/wind-trace-typ/wind-js/raw/master/自嘲嘲笑.js"
```
5.  随机图片

```
curl -o "./plugins/example/随机图片.js" "https://gitee.com/wind-trace-typ/wind-js/raw/master/随机图片.js"
//下面是图片资源拉取，本图库大部分都为龙图，自行决定是否使用，如不使用请在机器人根目录下的resources目录中创建一个名为wind-img的文件夹
git clone https://gitee.com/wind-trace-typ/wind-img.git ./resources/wind-img
```
6.  做菜系统

```
curl -o "./plugins/example/做菜系统.js" "https://gitee.com/wind-trace-typ/wind-js/raw/master/做菜系统.js"
```

7.  转大图

```
curl -o "./plugins/example/转大图.js" "https://gitee.com/wind-trace-typ/wind-js/raw/master/转大图.js"
```

8.  qq估价.js

```
curl -o "./plugins/example/qq估价.js" "https://gitee.com/wind-trace-typ/wind-js/raw/master/qq估价.js"
```

8.  qq估价.js


```
curl -o "./plugins/example/qq估价.js" "https://gitee.com/wind-trace-typ/wind-js/raw/master/信息查询.js"
```


#### 使用说明

1.  各大榜单及动漫图查询.js

| 指令  | 功能描述  |
|---|---|
| 群头像/群头像1234  | 获取当前群聊或指定群聊的头像  |
| 空间头像/空间头像@群友/空间头像1234  | 获取自己，群友或指定账号的空间头像   |
| bilibili周榜/哔哩哔哩周榜  | 获取哔哩哔哩周榜信息和链接，后跟数字也可自定义期数  |
| bilibili热搜/bilibili热搜榜/哔哩哔哩热搜  | 获取bilibili热搜榜单  |
| 抖音热搜/抖音热搜榜/tiktok热搜  | 获取抖音热搜榜单  |
| 微博热搜/微博热搜榜 | 获取微博热搜榜单  |
| 知乎热搜/知乎热搜榜 | 获取知乎热搜榜单  |
| 今日头条热搜/今日头条热搜榜 | 获取今日头条热搜榜单  |
| 今日头条新闻/今日头条热点新闻 | 获取今日头条热点新闻  |
| 随机动漫图/随机acg/acg/动漫图  | 获取随机动漫图  |
| 二次元头像/动漫头像/头像  | 获取二次元头像  |
| pc壁纸/电脑壁纸  | 获取pc端的动漫图  |
| 手机动漫图/手机壁纸  | 获取手机端的动漫图  |

2.  戳我戳他.js

| 指令  | 功能描述  |
|---|---|
| 戳我/戳我4下  | 让机器人自己4下  |
| 戳他/戳她/戳它/戳他5下/戳她3下/戳它2下  | 让机器人戳别人自定义次数  |


3.  才字诀

只要以'你'开头的句子都会触发，替换成'你才'开头，开启艾特触发后只有艾特机器人才会触发

4.  自嘲嘲笑.js

| 指令  | 功能描述  |
|---|---|
| 自嘲/自嘲@群友  | 发送自己嘲笑自己的伪装聊天记录  |
| 嘲笑/嘲笑@群友  | 发送自己嘲笑自己的伪装聊天记录  |

![功能展示](IMG_20230918_153524.jpg)

5.   随机图片.js

| 指令  | 功能描述  |
|---|---|
| 随机图片  | 获取Miao-Yunzai/resources/wind-img或Yunzai-Bot/resources/wind-img文件夹中的图片随机发送(可自行修改成其他正则)  |

6.   做菜系统.js

| 指令  | 功能描述  |
|---|---|
| 做菜+菜名/做菜+菜名+数量  | 获取菜的做法，以聊天记录形式发出，不带数量默认返回5个菜的做法，最高只能查10个菜的做法  |

7.   转大图.js

| 指令  | 功能描述  |
|---|---|
| 转大图+外显词条+图片  | 将你选中的图片以大图卡片的形式发送出来  |

具体操作演示如下，下面是带外显词条的情况(不加外显词条也可以，可以在js文件中修改默认外显词条)
![操作演示](Screenshot_2023-09-02-12-21-24-94.jpg)

8.   qq估价.js

| 指令  | 功能描述  |
|---|---|
| qq估价/qq估价@群友/qq估价123456  | 对你或者群友的qq进行估价  |

9.   信息查询.js

| 指令  | 功能描述  |
|---|---|
| 信息查询/信息查询@群友/信息查询+qq号 | 查询你或者群友的qq名片信息  |

#### 参与贡献

1.  作者qq：[459521475](https://tenapi.cn/v2/qqcard?qq=459521475)
2.  交流群：[761464165](https://qm.qq.com/cgi-bin/qm/qr?k=HUsXvhqzxSiGUCKQclf3NbcXp5gsC4h3&jump_from=webapi&authKey=/PHLSFBIFLvTOJTrtzNdaF1xmdkjglqVD4U8w90CBDJoGpDPqsGkUSzHSET7/Ykh)
3.  特别感谢[桑帛云API](https://api.lolimi.cn/)，[TenAPI](https://docs.tenapi.cn/),以及[FREE API](https://www.free-api.com/)提供的免费api

