import plugin from '../../lib/plugins/plugin.js';
import fetch from "node-fetch";
import { segment } from 'oicq';
import fs from 'fs';
//作者：风 qq：459521475
//可以添加其他图片，只需要添加在Miao-Yunzai/resources/wind-img文件夹下就可以
//如果没有该文件夹就重新新建一个，然后把图片放进去即可
//指令正则也可自行修改
export class example extends plugin {
  constructor() {
    super({
	  /** 功能名称 */
      name: '随机图片',
      /** 功能描述 */
      dsc: '发送随机的图片',
      /** https://oicqjs.github.io/oicq/#events */
      event: 'message',
      /** 优先级，数字越小等级越高 */
      priority: 1,
      rule: [
        {
          reg: "^#?随机图片$",
          fnc: 'randomimg'
        }
      ]
    });
  }

  async randomimg(e){
    const path = process.cwd() + '/resources/wind-img/';
    const files = fs.readdirSync(path);
    const imageFiles = files.filter(file => file.endsWith('.jpg') || file.endsWith('.png'));
    const randomIndex = Math.floor(Math.random() * imageFiles.length);
    const imagePath = `${path}/${imageFiles[randomIndex]}`;
    const img = fs.readFileSync(imagePath);
    await this.reply(segment.image(img));
  }

}