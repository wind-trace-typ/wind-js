import plugin from'../../lib/plugins/plugin.js'
import{segment}from'oicq'
import fs from 'fs';
import cfg from'../../lib/config/config.js'
import common from'../../lib/common/common.js'
import{uploadRecord}from '../yenai-plugin/model/index.js'
const path=process.cwd()
//在这里设置事件概率,请保证概率加起来小于1，少于1的部分会触发反击
let reply_text = 0.15 //文字回复概率
let reply_img = 0.3 //图片回复概率
let reply_voice = 0.4 //语音回复概率
let mutepick = 0.05//禁言概率
//剩下的0.1概率就是反击
const speaker='纳西妲'//自定义语音回复的角色
const api='https://api.pearktrue.cn/api/genshinimpactaudio/'
//定义图片存放路径 默认是Yunzai-Bot/resources/chuochuo
//图片不需要修改名称，直接放进chuochuo文件夹里即可
//文字回复的列表，可自定义，添加和删除都可
let word_list=['旅行者副本零掉落，旅行者深渊打不过，旅行者抽卡全保底，旅行者小保底必歪',
'不要再戳了！我真的要被你气死了！！！',
'怎么会有你这么无聊的人啊！！！',
'行不行啊细狗！！',
'是不是要我揍你一顿你才开心啊！！！',
'你干嘛哈哈哟',
'你可以舔小白露的jio吗~',
'你戳谁呢！你戳谁呢！！！',
'小朋友别戳了'];
//语音回复列表，支持自定义
let voice_list = ['旅行者副本零掉落，旅行者深渊打不过，旅行者抽卡全保底，旅行者小保底必歪',
    '不要再戳了！我真的要被你气死了！！！',
    '怎么会有你这么无聊的人啊！！！',
    '行不行啊细狗！！',
    '是不是要我揍你一顿你才开心啊！！！',
    '你干嘛哈哈哟',
    '你可以舔小白露的jio吗~',
    '你戳谁呢！你戳谁呢！！！',
    '小朋友别戳了'];

export class chuo extends plugin{
    constructor(){
    super({
        name: '戳一戳',
        dsc: '戳一戳机器人触发效果',
        event: 'notice.group.poke',
        priority: 5000,
        rule: [
            {
                /** 命令正则匹配 */
                fnc: 'chuoyichuo'
                }
            ]
        }
    )
}


async chuoyichuo (e){
    logger.info('[戳一戳生效]')
    if(e.target_id == cfg.qq){
        //生成0-100的随机数
        let random_type = Math.random()
        
        //回复随机文字
        if(random_type < reply_text){
            let text_number = Math.ceil(Math.random() * word_list['length'])
            return await e.reply(word_list[text_number-1])
        }
        
        
        //回复随机图片
        else if(random_type < (reply_text + reply_img)){

            const path = process.cwd() + '/resources/chuochuo/'; // 图片所在文件夹路径
			const files = fs.readdirSync(path); // 获取文件夹下的所有文件
			const imageFiles = files.filter(file => file.endsWith('.jpg') || file.endsWith('.png')); // 过滤出图片文件
			const randomIndex = Math.floor(Math.random() * imageFiles.length); // 随机选择一张图片
			const imagePath = `${path}/${imageFiles[randomIndex]}`; // 图片路径
			const img = fs.readFileSync(imagePath); // 读取图片
			return await this.reply(segment.image(img)); // 发送图片

        }
        
        //回复随机语音
        else if(random_type < (reply_text + reply_img + reply_voice)){
            let voice_number = Math.ceil(Math.random() * voice_list['length'])
            let text = voice_list[voice_number-1]
            let url=api+`?text=${text}&speaker=${speaker}`
            let res = await fetch(url).catch((err) => logger.error(err));
            res = await res.json();
            if(res.code==200){
                return await e.reply(await uploadRecord(res.audiourl, 0, false));
            }
        }
        
        //禁言
        else if(random_type < (reply_text + reply_img + reply_voice + mutepick)){
            //两种禁言方式，随机选一种
            let mutetype = Math.ceil(Math.random() * 2)
            if(mutetype == 1){
                e.reply('说了不要戳了！')
                await common.sleep(1000)
                await e.group.muteMember(e.operator_id,60);
                await common.sleep(3000)
                e.reply('啧')
                //有这个路径的图话可以加上
                await e.reply(segment.image('file:///' + path + '/resources/chuochuo/'+'laugh.jpg'))
            }
            else if (mutetype == 2){
                e.reply('不！！')
                await common.sleep(500);
                e.reply('准！！')
                await common.sleep(500);
                e.reply('戳！！')
                await common.sleep(1000);
                await e.group.muteMember(e.operator_id,60)
            }
            return true;
        }
        
        //反击
        e.reply('反击！')
        await common.sleep(1000)
        await e.group.pokeMember(e.operator_id)}
        return true;
}
    
}
